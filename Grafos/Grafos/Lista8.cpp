#include "Lista8.h"



Lista8::Lista8()
{
	primero = NULL;
	ultimo = NULL;
}


Lista8::~Lista8()
{
}

void Lista8::insertarPrincipio(int elemento)
{
	if (primero == NULL)
	{
		primero = new NodoLista<int>;
		primero->setElemento(elemento);
		ultimo = primero; //primero y ultimo son solo punteros
	}
	else
	{
		NodoLista<int>* nuevo;
		nuevo = new NodoLista<int>;
		nuevo->setElemento(elemento);
		nuevo->setSiguiente(primero);
		primero->setAnterior(nuevo);
		primero = nuevo;
	}
}

void Lista8::insertarFinal(int elemento)
{
	if (ultimo == NULL)
	{
		ultimo = new NodoLista<int>;
		ultimo->setElemento(elemento);
		primero = ultimo;
	}
	else
	{
		NodoLista<int>*nuevo;
		nuevo = new NodoLista<int>;
		nuevo->setElemento(elemento);
		nuevo->setAnterior(ultimo);
		ultimo->setSiguiente(nuevo);
		ultimo = nuevo;
	}
}

void Lista8::eliminarPrincipio()
{
	if (primero != NULL)
	{
		if (primero == ultimo)
		{
			delete primero;
			primero = NULL;
			ultimo = NULL;
		}
		else
		{
			NodoLista<int>*aux;
			aux = primero->getSiguiente();
			delete primero;
			primero = aux;
			primero->setAnterior(NULL);
		}
	}
}

void Lista8::eliminarFinal()
{
	if (ultimo != NULL)
	{
		if (ultimo == primero)
		{
			delete ultimo;
			primero = NULL;
			ultimo = NULL;
		}
		else
		{
			NodoLista<int>*aux;
			aux = ultimo->getAnterior();
			delete ultimo;
			ultimo = aux;
			ultimo->setSiguiente(NULL);
		}
	}
}

void Lista8::mostrarLista()
{
	NodoLista<int>* aux;
	aux = primero;
	if (ultimo != NULL && primero != NULL)
	{
		cout << "Lista: {";
		while (aux != ultimo)
		{
			cout << aux->getElemento();
			cout << ",";
			aux = aux->getSiguiente();
		}
		cout << ultimo->getElemento();
		cout << "}" << endl;
	}
	else
		cout << "Lista Vacia" << endl;
}

int Lista8::encontrarElementoPosicion(int pos)
{
	NodoLista<int>*aux;
	int cont = 1;
	aux = primero;
	while (aux != NULL)
	{
		if (cont == pos)
			return aux->getElemento();
		cont++;
		aux = aux->getSiguiente();
	}
	return NULL;
}

NodoLista<int>* Lista8::buscarElemento(int elemento)
{
	NodoLista<int>*aux;
	aux = primero;
	while (aux != NULL)
	{
		if (aux->getElemento() == elemento)
			return aux;
		aux = aux->getSiguiente();
	}
	return NULL;
}

void Lista8::eliminarElementoDado(int elemento)
{
	if (primero != NULL && ultimo != NULL)
	{
		if (elemento == primero->getElemento())
			eliminarPrincipio();
		else
		{
			if (elemento == ultimo->getElemento())
				eliminarFinal();
			else
			{
				NodoLista<int>*aux = primero;
				while (elemento != aux->getElemento() && aux != ultimo)
				{
					aux = aux->getSiguiente();
				}
				aux->getAnterior()->setSiguiente(aux->getSiguiente());
				aux->getSiguiente()->setAnterior(aux->getAnterior());
				delete aux;
			}
		}
	}
	else
		cout << "Lista Vacia" << endl;
}

bool Lista8::verifVacio()
{
	if (primero == NULL && ultimo == NULL)
		return true;
	else
		return false;
}
