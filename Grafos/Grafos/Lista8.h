#pragma once
#include "NodoLista.h"
class Lista8
{
private:
	NodoLista<int> * primero;
	NodoLista<int> * ultimo;
public:
	Lista8();
	~Lista8();
	void insertarPrincipio(int elemento);
	void insertarFinal(int elemento);
	void eliminarPrincipio();
	void eliminarFinal();
	void mostrarLista();
	int encontrarElementoPosicion(int pos);
	NodoLista<int>* buscarElemento(int elemento);
	void eliminarElementoDado(int elemento);
	bool verifVacio();
};

