#pragma once
#include "NodoTUAD.h"
#include <fstream>
#define TAM 10
class TUADGrafo
{
private:
	NodoTUAD grafo[TAM];
	Lista8 cola;
	fstream arch;
public:
	TUADGrafo();
	~TUADGrafo();
	void ingresarArchivo();
	void inicializar();
	void insertarAristaNodo(int nodo,int arista);
	void eliminarAristaNodo(int nodo, int arsita);
	void mostrarNodos();
	void modificarPadreNodo(int nodo,int padre);
	void modificarMarcaNodo(int nodo, bool marca);
	int encontrarAristaPosicion(int nodo,int pos);
	bool busquedaEnAmplitud(int origen, int destino);
	bool busquedaEnProfundidad(int origen, int destino);
	void amplitudTodosNodos(int origen);
	void menu();
};

