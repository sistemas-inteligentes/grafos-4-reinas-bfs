#include "TUADGrafo.h"



TUADGrafo::TUADGrafo()
{
	arch.open("D:\\Universidad\\Cuarto_Semestre\\Estructuras de Datos\\aristas.txt", ios::in);
}


TUADGrafo::~TUADGrafo()
{
	arch.close();
}

void TUADGrafo::ingresarArchivo()
{
	int nodo, arista;
	while (arch >> nodo >> arista)
	{
		insertarAristaNodo(nodo, arista);
	}
}

void TUADGrafo::inicializar()
{
	for (int i = 1; i < TAM; i++)
	{
		grafo[i].setPadre(-1);
		grafo[i].setMarcar(false);
	}
}

void TUADGrafo::insertarAristaNodo(int nodo, int arista)
{
	grafo[nodo].setExiste(true);
	grafo[nodo].nuevaArista(arista);
}

void TUADGrafo::eliminarAristaNodo(int nodo, int arsita)
{
	grafo[nodo].eliminarAristaDada(arsita);
}

void TUADGrafo::mostrarNodos()
{
	int i = 1;
	while (i<TAM)
	{
		cout << "Nodo: " << i <<" Padre: "<<grafo[i].getPadre()<<" Marca: "<<grafo[i].getMarca()<< " = Aristas: ";
		grafo[i].getArista()->mostrarLista();
		i++;
	}
}

void TUADGrafo::modificarPadreNodo(int nodo, int padre)
{
	grafo[nodo].setPadre(padre);
}

void TUADGrafo::modificarMarcaNodo(int nodo, bool marca)
{
	grafo[nodo].setMarcar(marca);
}

int TUADGrafo::encontrarAristaPosicion(int nodo,int pos)
{
	return grafo[nodo].getArista()->encontrarElementoPosicion(pos);
}

bool TUADGrafo::busquedaEnAmplitud(int origen, int destino)
{
	inicializar();
	bool encontre = false;
	grafo[origen].setMarcar(true);
	cola.insertarFinal(origen);
	while (cola.verifVacio() == false && encontre == false)
	{
		int iesimo = 1;
		int vert = cola.encontrarElementoPosicion(1);
		cola.eliminarPrincipio();
		int adya = grafo[vert].getArista()->encontrarElementoPosicion(iesimo);
		while (adya != 0 && encontre == false)
		{
			if (grafo[adya].getMarca() == false)
			{
				grafo[adya].setPadre(vert);
				if (adya == destino)
					encontre = true;
				else
				{
					grafo[adya].setMarcar(true);
					//cola.eliminarPrincipio();
					cola.insertarFinal(adya);
				}
			}
			iesimo = iesimo + 1;
			adya = grafo[vert].getArista()->encontrarElementoPosicion(iesimo);
		}
	}
	return encontre;
}

bool TUADGrafo::busquedaEnProfundidad(int origen, int destino)
{
	bool encontro = false;
	grafo[origen].setMarcar(true);
	int iesimo = 1;
	int adya = grafo[origen].encontrarAristaDada(iesimo);
	while (adya != 0)
	{
		if (encontro == false && grafo[adya].getMarca() == false)
		{
			grafo[adya].setPadre(origen);
			if (adya == destino)
				encontro = true;
			else
				encontro = busquedaEnProfundidad(adya, destino);
		}
		iesimo = iesimo + 1;
		adya = grafo[origen].encontrarAristaDada(iesimo);
	}
	return encontro;
}

void TUADGrafo::amplitudTodosNodos(int origen)
{
	if (origen <= 0)
		cout << "Origen no valido" << endl;
	else
	{
		for (int i = 1; i < TAM; i++)
		{
			if (i != origen)
			{
				bool res = busquedaEnAmplitud(origen, i);
				if (res)
				{
					cout << "Camino mas corto de "<<origen <<" a "<<i<<": ";
					int res = i;
					while (res != -1)
					{
						cout << res << ",";
						res = grafo[res].getPadre();
					}
					cout << endl;
				}
			}
		}
	}
}

void TUADGrafo::menu()
{
	int op;
	do
	{
		cout << "ELIJA UNA OPCION" << endl;
		cout << "1. INSERTAR NUEVA ARISTA A NODO" << endl;
		cout << "2. ENCONTRAR ARISTA POR POSICION" << endl;
		cout << "3. ELIMINAR ARISTA DE NODO" << endl;
		cout << "4. MOSTRAR NODOS" << endl;
		cout << "5. BUSQUEDA EN AMPLITUD" << endl;
		cout << "6. BUSQUEDA EN PROFUNDIDAD" << endl;
		cout << "7. AMPLITUD A TODOS LOS NODOS" << endl;
		cout << "8. INGRESAR DESDE ARCHIVO" << endl;
		cout << "9. SALIR" << endl;
		cin >> op;
		switch (op)
		{
		case 1:
		{
			int nodo, arista;
			cout << "Ingrese nodo: ";
			cin >> nodo;
			cout << "Ingrese arista: ";
			cin >> arista;
			insertarAristaNodo(nodo, arista);
		}
		break;
		case 2:
		{
			int nodo, arista;
			cout << "Ingrese nodo: ";
			cin >> nodo;
			cout << "Ingrese posicion: ";
			cin >> arista;
			int e=encontrarAristaPosicion(nodo, arista);
			if (e != 0)
				cout << "Arista en pos " << arista << ": " << e << endl;
			else
				cout << "Arista no encontrada" << endl;
		}
		break;
		case 3:
		{
			int nodo, arista;
			cout << "Ingrese nodo: ";
			cin >> nodo;
			cout << "Ingrese arista a eliminar: ";
			cin >> arista;
			eliminarAristaNodo(nodo, arista);
		}
		break;
		case 4:
		{

			//inicializar();
			mostrarNodos();
		}
		break;
		case 5:
		{
			inicializar();
			int origen, destino;
			cout << "Ingrese origen: ";
			cin >> origen;
			cout << "Ingrese destino: ";
			cin >> destino;
			bool res = busquedaEnAmplitud(origen, destino);
			if (res == true)
			{
				cout << "Camino encontrado" << endl;
				cout << "Camino mas corto: ";
				int res = destino;
				while (res != -1)
				{
					cout << res << ",";
					res = grafo[res].getPadre();
				}
				cout << endl;
			}
			else
				cout << "Camino no encontrado" << endl;
		}
		break;
		case 6:
		{
			inicializar();
			int origen, destino;
			cout << "Ingrese origen: ";
			cin >> origen;
			cout << "Ingrese destino: ";
			cin >> destino;
			bool res = busquedaEnProfundidad(origen, destino);
			if (res == true)
			{
				cout << "Camino encontrado" << endl;
				cout << "Camino: ";
				int res = destino;
				while (res != -1)
				{
					cout << res << ",";
					res = grafo[res].getPadre();
				}
				cout << endl;
			}
			else
				cout << "Camino no encontrado" << endl;
		}
		break;
		case 7:
		{
			int origen;
			cout << "Ingrese origen: ";
			cin >> origen;
			amplitudTodosNodos(origen);
		}
		case 8:
			ingresarArchivo();
			break;
		break;
		default:
			break;
		}
	} while (op != 9);
}
