#pragma once
#include <iostream>
#include <string>
using namespace std;

template <class T>

class NodoLista
{
private:
	T elemento;
	NodoLista<T> * anterior;
	NodoLista<T> * siguiente;
public:
	NodoLista();
	~NodoLista();
	void setElemento(T elemento);
	void setAnterior(NodoLista* anterior);
	void setSiguiente(NodoLista* siguiente);
	T getElemento();
	NodoLista<T>* getAnterior();
	NodoLista<T>* getSiguiente();
};

template<class T>
inline NodoLista<T>::NodoLista()
{
	anterior = NULL;
	siguiente = NULL;
}

template<class T>
inline NodoLista<T>::~NodoLista()
{
}

template<class T>
inline void NodoLista<T>::setElemento(T elemento)
{
	this->elemento = elemento;
}

template<class T>
inline void NodoLista<T>::setAnterior(NodoLista * anterior)
{
	this->anterior = anterior;
}

template<class T>
inline void NodoLista<T>::setSiguiente(NodoLista * siguiente)
{
	this->siguiente = siguiente;
}

template<class T>
inline T NodoLista<T>::getElemento()
{
	return elemento;
}

template<class T>
inline NodoLista<T> * NodoLista<T>::getAnterior()
{
	return anterior;
}

template<class T>
inline NodoLista<T> * NodoLista<T>::getSiguiente()
{
	return siguiente;
}
