#include "NodoTUAD.h"



NodoTUAD::NodoTUAD()
{
	padre = -1;
	marca = false;
	aristas = new Lista8;
}


NodoTUAD::~NodoTUAD()
{
}

void NodoTUAD::inicializar()
{
	this->padre = -1;
	this->marca = false;
}

void NodoTUAD::nuevaArista(int arista)
{
	aristas->insertarFinal(arista);
}

void NodoTUAD::setPadre(int padre)
{
	this->padre = padre;
}

void NodoTUAD::setMarcar(bool marca)
{
	this->marca = marca;
}

int NodoTUAD::encontrarAristaDada(int arista)
{
	return aristas->encontrarElementoPosicion(arista);
}

void NodoTUAD::eliminarAristaDada(int arista)
{
	aristas->eliminarElementoDado(arista);
}

int NodoTUAD::getPadre()
{
	return padre;
}

bool NodoTUAD::getMarca()
{
	return marca;
}

bool NodoTUAD::getExiste()
{
	return existe;
}

void NodoTUAD::setExiste(bool existe)
{
	this->existe = existe;
}

Lista8* NodoTUAD::getArista()
{
	return aristas;
}
