#pragma once
#include "Lista8.h"

class NodoTUAD
{
private:
	int padre;
	bool marca;
	Lista8 *aristas;
	bool existe;
public:
	NodoTUAD();
	~NodoTUAD();
	void inicializar();
	void nuevaArista(int arista);
	void setPadre(int padre);
	void setMarcar(bool marca);
	int encontrarAristaDada(int arista);
	void eliminarAristaDada(int arista);
	int getPadre();
	bool getMarca();
	bool getExiste();
	void setExiste(bool existe);
	Lista8* getArista();
};

